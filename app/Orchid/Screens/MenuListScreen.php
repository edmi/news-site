<?php

namespace App\Orchid\Screens;

use App\Models\Category;
use App\Orchid\Layouts\MenuEditLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\ModalToggle;
use Orchid\Screen\TD;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Toast;

class MenuListScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'category' => Category::all()
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Категории меню на сайте';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            ModalToggle::make('Добавить категорию')
                ->modal('asyncEditMenuModal')
                ->modalTitle('Добавить категорию')
                ->method('saveCategory')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::table('category', [
                TD::make('id')->sort(),
                TD::make('name')->render(function (Category $category){
                    return ModalToggle::make($category->name)
                        ->modal('asyncEditMenuModal')
                        ->modalTitle($category->name)
                        ->method('saveCategory')
                        ->asyncParameters([
                            'category' => $category->id,
                        ]);
                }),
                TD::make('slug'),
            ]),

            Layout::modal('asyncEditMenuModal', MenuEditLayout::class)
                ->async('asyncGetCategory'),
        ];
    }
    public function asyncGetCategory(Category $category): iterable
    {
        return [
            'category' => $category,
        ];
    }

    /**
     * @param Request $request
     * @param Category $category
     */
    public function saveCategory(Request $request, Category $category): void
    {
        $category->fill($request->input('category'))->save();

        Toast::info(__('category was saved.'));
    }
}
