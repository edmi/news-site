<?php

namespace App\Orchid\Screens;

use App\Models\Category;
use App\Models\Post;
use App\Models\postLang;
use App\Models\PostLanguage;
use App\Orchid\Layouts\PostTranslateableLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;
use Stichoza\GoogleTranslate\GoogleTranslate;

class PostAutoCreateScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'PostAutoCreateScreen';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Save post')
                ->icon('pencil')
                ->method('autocreate')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            PostTranslateableLayout::class,
            Layout::rows([
                Select::make('post.category_id')->horizontal()
                    ->fromModel(Category::class, 'name')
                    ->title('Категория'),
                Cropper::make('post.image')
                    ->title('Large web banner image, generally in the front and center')
                    ->maxWidth(1920)
                    ->maxHeight(1080)
                    ->targetId()
            ])
        ];
    }

    public function autocreate(Post $post, Request $request)
    {
        $tr = (new GoogleTranslate());
        $inputPost = $request->get('post');
        $tr->setSource($inputPost['translateFrom']);
        $title = $inputPost['title'];

        $body = strip_tags($inputPost['body'], ['p', 'b', 'i', 'h2', 'h3', 'blockquote', 'ul', 'ol', 'li', 'table', 'td', 'th', 'tr', 'img']);

        try {
            $post->fill($request->get('post'));
            $post->author = auth()->id();
            $tr->setTarget(postLang::DEFAULT_LANGUAGE);
            $post->title = $tr->translate($title);
            $post->body = preg_replace('/\/\s/', '/', preg_replace('/\sclass=[\'|"][^\'"]+[\'|"]/', '', $tr->translate($body)));
            $post->save();
            $post->attachment()->syncWithoutDetaching(
                $request->input('post.attachment', [])
            );
            /*calc width/height for image*/
            $post->calcImageParam();

            foreach (postLang::LANGUAGES as $key => $locale) {
                $tr->setTarget($key);
                $lang = new PostLanguage;
                $lang->title = $tr->translate($title);
                $lang->body = preg_replace('/\/\s/', '/', $tr->translate($body));
                $lang->language = $key;
                $lang->post_id = $post->id;
                $lang->save();
            }

        } catch (\Exception $exception) {
            Alert::error('Post create and save error');
        }
    }

}
