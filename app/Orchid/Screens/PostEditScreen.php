<?php

namespace App\Orchid\Screens;

use App\Models\Category;
use App\Models\Post;
use App\Models\PostAttribute;
use App\Models\postLang;
use App\Models\PostLanguage;
use App\Models\User;
use App\Orchid\Layouts\PostTranslateableLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\Upload;
use Orchid\Support\Facades\Alert;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Screen;

class PostEditScreen extends Screen
{
    public $post;
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Creating a new post';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Blog posts';

    /**
     * @var bool
     */
    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Post $post): array
    {
        if ($post->exists) {
            $this->name = 'Edit post';
        }

        return [
            'post' => $post
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Create post')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->post->exists),

            Button::make('Update')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->post->exists),

            Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->post->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        $langsArray = $this->post->languages;
        $manyForms = ['IT (default)' => PostTranslateableLayout::class];
        $attr_rows = [];
        $attributes = $this->post->attributes;

        $reserv = 0;
        foreach (postLang::LANGUAGES as $key => $name) {
            $lang = $langsArray->where('language', $key);
            $id = array_key_first($lang->toArray()) ?? $reserv;

            $manyForms[$name] = Layout::rows([

                Input::make('post.languages.' . $id . '.language')->value($key)->hidden(),
                Input::make('post.languages.' . $id . '.id')->hidden(),
                Input::make('post.languages.' . $id . '.post_id')->hidden(),
                Input::make('post.languages.' . $id . '.title')
                    ->title('Title')
                    ->placeholder('Attractive but mysterious title')
                    ->help('Specify a short descriptive title for this post.'),
                Quill::make('post.languages.' . $id . '.body')
                    ->title('Main text')
            ]);
            $reserv++;
        }


        $attr_exclude = [];
        foreach ($attributes as $id => $attribute) {
            $attr_rows[] = Input::make('post.attributes.' . $id . '.data')
                ->title(array_flip(PostAttribute::ATTRIBUTE_ARRAY)[$attribute->attribute_id] ?? 'error_title');
            $attr_rows[] = Input::make('post.attributes.' . $id . '.id')->hidden();
            $attr_rows[] = Input::make('post.attributes.' . $id . '.attribute_id')->value($attribute->attribute_id)->hidden();
            $attr_exclude[] = $attribute->attribute_id;
        }

        $reserv = count($attr_exclude);
        foreach (PostAttribute::ATTRIBUTE_ARRAY as $name => $attributeId) {
            if (!in_array($attributeId, $attr_exclude)) {
                $attr_rows[] = Input::make('post.attributes.' . $reserv . '.data')
                    ->title($name);
                $attr_rows[] = Input::make('post.attributes.' . $reserv . '.id')->hidden();
                $attr_rows[] = Input::make('post.attributes.' . $reserv . '.attribute_id')->value($attributeId)->hidden();
                $reserv++;
            }
        }
        return [
            Layout::tabs($manyForms),
            Layout::rows([
                    Select::make('post.category_id')->horizontal()
                        ->fromModel(Category::class, 'name')
                        ->title('Категория'),
                    Cropper::make('post.image')
                        ->title('Large web banner image, generally in the front and center')
                        ->width(1000)
                        ->height(500)
                        ->targetId(),

                    Relation::make('post.author')
                        ->title('Author')
                        ->fromModel(User::class, 'name'),

                ]
            ),
            Layout::accordion([
                'Images Upload' => Layout::rows([
                    Upload::make('post.attachment')
                        ->title('All files'),
                ]),
                'addditional fields' => [Layout::rows($attr_rows)]
            ]),

        ];
    }

    /**
     * @param Post $post
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Post $post, Request $request)
    {
        try {
            $post->fill($request->get('post'));
            $post->author = auth()->id();
            $post->save();
            $post->attachment()->syncWithoutDetaching(
                $request->input('post.attachment', [])
            );
            /*calc width/height for image*/
            $post->calcImageParam();
            foreach ($request->input('post.languages') as $inputLang) {
                if (!empty($inputLang['id'])) {
                    $lang = PostLanguage::whereId($inputLang['id'])->first()->fill($inputLang);
                } else {
                    $lang = (new PostLanguage)->fill($inputLang);
                    $lang->post_id = $post->id;
                }
                $lang->save();
            }

            foreach ($request->input('post.attributes') as $attribute) {
                if (!empty($attribute['id']) && empty($attribute['data'])) {
                    PostAttribute::whereId($attribute['id'])->delete();
                }
                if (!empty($attribute['data'])) {
                    $attribute['post_id'] = $post->id;
                    PostAttribute::whereId($attribute['id'])->firstOrNew()->fill($attribute)->save();
                }
            }

        } catch (\Exception $exception) {
            Alert::error('Post create and save error');
        }

        Alert::info('You have successfully created an post.');
        return redirect()->route('platform.post.list');
    }

    /**
     * @param Post $post
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Post $post)
    {
        $post->delete();

        Alert::info('You have successfully deleted the post.');

        return redirect()->route('platform.post.list');
    }
}
