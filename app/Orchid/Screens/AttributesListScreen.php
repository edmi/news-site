<?php

namespace App\Orchid\Screens;

use App\Models\Attributes;
use App\Models\Category;
use App\Orchid\Layouts\AttributesEditLayout;
use App\Orchid\Layouts\MenuEditLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\ModalToggle;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class AttributesListScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'attribute' => Attributes::all()
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Дополнительные поля для постов';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            ModalToggle::make('Добавить поле атрибута')
                ->modal('asyncEditAttrModal')
                ->modalTitle('Добавить поле атрибута')
                ->method('saveAttr')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::table('attribute', [
                TD::make('id')->sort(),
                TD::make('name')->render(function (Attributes $attributes){
                    return ModalToggle::make($attributes->name)
                        ->modal('asyncEditMenuModal')
                        ->modalTitle($attributes->name)
                        ->method('saveAttr')
                        ->asyncParameters([
                            'attribute' => $attributes->id,
                        ]);
                }),
                TD::make('key'),
            ]),

            Layout::modal('asyncEditAttrModal', AttributesEditLayout::class)
                ->async('asyncGetAttr'),
        ];
    }
    public function asyncGetAttr(Attributes $attributes): iterable
    {
        return [
            'category' => $attributes,
        ];
    }

    /**
     * @param Request $request
     * @param Category $category
     */
    public function saveAttr(Request $request, Attributes $attributes): void
    {
        $attributes->fill($request->input('attribute'))->save();

        Toast::info(__('category was saved.'));
    }
}
