<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layouts\Rows;
use Orchid\Support\Facades\Layout;

class PostTranslateableLayout extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): iterable
    {
        return [

            Group::make([
                Input::make('post.title')
                ->title('Title')
                ->placeholder('Attractive but mysterious title')
                ->horizontal()
                ->help('Specify a short descriptive title for this post.'),
                Select::make('post.translateFrom')->title('Язык перевода')
                    ->horizontal()
                    ->options(['ru'=>'ru', 'en'=>'en'])
            ]),


            TextArea::make('post.description')
                ->title('Description')
                ->rows(3)
                ->maxlength(200)
                ->placeholder('Brief description for preview'),

            Quill::make('post.body')
                ->title('Main text')
        ];
    }
}
