<?php

namespace App\Orchid\Presenters;

use App\Models\postLang;
use Carbon\Carbon;
use Orchid\Support\Presenter;

class PostPresenter extends Presenter
{
    public function title(): string
    {
        if (app()->getLocale() !== postLang::DEFAULT_LANGUAGE) {
            return $this->entity->language->title ?? '';
        }
        return $this->entity->title;
    }

    public function created_at(): string
    {
        return Carbon::parse($this->entity->created_at)->translatedFormat('M d Y');
    }

    public function body(): string
    {
        if (app()->getLocale() !== postLang::DEFAULT_LANGUAGE) {
            return $this->entity->language->body ?? '' ;
        }
        return $this->entity->body;
    }
}
