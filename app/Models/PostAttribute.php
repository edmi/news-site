<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostAttribute extends Model
{
    use HasFactory;
    public $timestamps = false;
    public const ATTRIBUTE_ARRAY = [
        'youtube' => 1,
        'link' => 2,
        'kb' => 3,
        'update' => 4,
    ];
    protected $fillable = [
        'post_id',
        'attribute_id',
        'data'
    ];
}
