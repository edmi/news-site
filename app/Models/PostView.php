<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Request;

class PostView extends Model
{
    use HasFactory;

    public static function createViewLog(Post $post)
    {
        if (isset($_SERVER['HTTP_USER_AGENT'])
            && preg_match('/apple|baidu|bingbot|facebookexternalhit|googlebot|-google|ia_archiver|msnbot|naverbot|pingdom|seznambot|slurp|teoma|twitter|yandex|yeti|bot|crawl|curl|dataprovider
|search|get|spider|find|java|majesticsEO|google|yahoo|contaxe|libwww-perl/i', $_SERVER['HTTP_USER_AGENT'])
        ) {
            return null;
        }

        if (!in_array($post->id, Post::SERVICE_PAGES)) {

            $postViews = new PostView();
            $postViews->post_id = $post->id;
            $postViews->titleslug = $post->title;
            $postViews->language = app()->getLocale();
            $postViews->url = Request::url();
            $postViews->ip = Request::getClientIp();
            $postViews->agent = Request::header('User-Agent');
            $postViews->save();
        }
    }
}
