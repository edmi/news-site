<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class postLang extends Model
{
    use HasFactory;
    public const DEFAULT_LANGUAGE = 'it';
    public const LANGUAGES = [
        'pt' => 'Portugues',
        'ko' => 'Korean',
        'ja' => 'Japan',
        'fr' => 'France',
        'de' => 'Deutsch'
        ];
}
