<?php

namespace App\Models;

use App\Orchid\Presenters\PostPresenter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Orchid\Attachment\Attachable;
use Orchid\Attachment\Models\Attachment;
use Orchid\Screen\AsSource;
use System\config;

class Post extends Model
{
    use HasFactory, AsSource, Attachable;

    const SERVICE_PAGES = [self::ABOUT_PAGE, self::PRIVACY_PAGE];
    const ABOUT_PAGE = 8908;
    const PRIVACY_PAGE = 8909;

    protected $appends = ['firstImage'];
    protected $fillable = [
        'title',
        'description',
        'keywords',
        'body',
        'slug',
        'image',
        'author',
        'category_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($p) {
            $p->slug = Str::slug($p->title);
        });
    }

    public function getFirstImageAttribute()
    {
        return self::image()->first() ?? self::getDefaultImage();
    }

    public function getDefaultImage()
    {
        $image = new Attachment();
        $image->name = config()->get('app.noimage');
        $image->path = asset(config()->get('app.noimage'));
        list($width, $height) = getimagesize($image->url(asset(config()->get('app.noimage'))));
        $image->height = $height;
        $image->width = $width;
        return $image;
    }

    public function postView()
    {
        return $this->hasMany(PostView::class);
    }

    public function language()
    {
        return $this->hasOne(PostLanguage::class)->where('language', app()->getLocale());
    }

    public function languages()
    {
        return $this->hasMany(PostLanguage::class);
    }

    public function attributes()
    {
        return $this->hasMany(PostAttribute::class);
    }

    public function image()
    {
        return $this->hasOne(Attachment::class, 'id', 'image');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public static function findBySlug(string $slug)
    {
        if (app()->getLocale() !== postLang::DEFAULT_LANGUAGE) {
            return self::whereHas('language')->where('slug', $slug)->with(['attributes', 'language'])->withCount('postView')->firstOrFail();
        } else
            return self::where('slug', $slug)->withCount('postView')->firstOrFail();
    }

    public static function getLatestByCategory(int $category_id, int $count = 15, string $locale = '')
    {
        if (app()->getLocale() !== postLang::DEFAULT_LANGUAGE) {
            return self::whereHas('language')->where('category_id', $category_id)->with(['image', 'category'])->latest()->paginate($count);
        } else
            return self::where('category_id', $category_id)->with(['image', 'category'])->latest()->paginate($count);
    }

    public static function trending($dateForTrends = false, $take = 6)
    {
        if ($dateForTrends) {
            $date = $dateForTrends * 24;
            $trendsDate = date("Y-m-d H:i:s", strtotime("- $date hours", time()));
            $posts = self::withCount(['postView' => function ($q) use ($trendsDate) {
                $q->where('post_views.created_at', '>=', $trendsDate);
            }]);
        } else {
            $posts = self::withCount('postView');
        }
        $posts->orderBy('post_view_count', 'desc');
        if (app()->getLocale() !== postLang::DEFAULT_LANGUAGE) {
            $posts->whereHas('language')->with(['language', 'category']);
        }
        return $posts->take($take)->get();
    }

    public function getPostUpdateAttribute()
    {
        if (!empty($attr = $this->attributes()->whereIn('attribute_id', [3, 4])->get())) {
            $update = [
                'link' => '',
                'kb' => ''
            ];
            foreach ($attr as $item) {
                if ($item->attribute_id == 4) {
                    $update['link'] = $item->data;
                }
                if ($item->attribute_id == 3) {
                    $update['kb'] = $item->data;
                }

            }
            return $update;
        }
        return false;
    }

    public function getPostKbAttribute()
    {
        if (!empty($attr = $this->attributes()->where('attribute_id', 3)->first())) {
            return $attr->data;
        }
        return false;
    }

    public function getPostDownloadAttribute()
    {
        if (!empty($attr = $this->attributes()->where('attribute_id', 4)->first())) {
            return $attr->data;
        }
        return false;
    }

    public function getPostYoutubeAttribute()
    {
        if (!empty($attr = $this->attributes()->where('attribute_id', 1)->first())) {
            return str_replace('youtu.be', 'youtube.com/embed', $attr->data);
        }
        return false;
    }

    public function setSlugAttribute($value)
    {
        if (static::whereSlug($slug = Str::slug($value))->exists()) {
            $slug = "{$slug}-{$this->id}";
        }
        $this->attributes['slug'] = $slug;
    }

    public function calcImageParam() {
        $image = $this->image()->first();
        if (!is_null($image)) {
            list($width, $height) = getimagesize($image->url());
            $image->height = $height;
            $image->width = $width;
            $image->save();
        }
    }
    public function presenter()
    {
        return new PostPresenter($this);
    }
}
