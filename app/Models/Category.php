<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

class Category extends Model
{
    use HasFactory, AsSource;
    const DEFAULT_TITLES = [
        'High Tech' => 'Interesting news from the world of high technology',
        'Science' => 'Latest science and technology news',
        'Car' => 'Car News - Latest news from the world of cars',
        'Game' => 'Latest gaming industry news',
        'Space' => 'Space news, astronomy and astrophysics',
        'Cryptocurrency' => 'Cryptocurrencies - latest news today',
    ];
    const DEFAULT_DESC = [
        'High Tech' => 'Science and high-tech news: the most interesting events in the high-tech world. The most up-to-date information and expert commentary.',
        'Science' => 'The most interesting news from world science, scientific discoveries, space mysteries, incredible natural phenomena.',
        'Car' => 'The most interesting and important news from the automotive world, test drives of popular models, car price reviews, exclusive materials and reviews',
        'Game' => 'Game news on PS4, PC (PC), Xbox One, PS5, Xbox Series X, Nintendo Switch, iOS, Android and other systems, devices and platforms.',
        'Space' => 'Discoveries and news from space, astronomy and astrophysics. Popular scientific articles and new theories. The best space photographs. New and reliable facts',
        'Cryptocurrency' => 'All the latest from the cryptocurrency industry - bitcoin, blockchain, mining.',
    ];
    const SERVICE_CATEGORY = 'page';
    protected $fillable = [
        'name',
        'slug',
    ];
    public function posts() {
        return $this->hasMany(Post::class, 'category_id', 'id');
    }
    public function latestposts() {
        $posts =$this->hasMany(Post::class, 'category_id', 'id')
            ->with(['image','category']);

        if (app()->getLocale() !== postLang::DEFAULT_LANGUAGE) {
            $posts = $posts->whereHas('language')->with(['image','language','category']);
        }

        return $posts->latest()->take(15)->get();
    }

}
