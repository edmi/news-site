<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Storage;
use Orchid\Platform\Events\UploadedFileEvent;

class UploadFileEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(UploadedFileEvent $event)
    {
        $attach = $event->attachment;
        $quality = 85;
        $path = Storage::disk('public')->path($attach->physicalPath());//$this->image->path();
        $info = getimagesize(Storage::disk('public')->path($attach->physicalPath()));
        $isAlpha = false;

        switch ($info['mime']) {
            case 'image/jpeg':
                $image = imagecreatefromjpeg($path);
                break;
            case 'image/gif':
                $isAlpha = true;
                $image = imagecreatefromgif($path);
                break;
            case 'image/png':
                $isAlpha = true;
                $image = imagecreatefrompng($path);
                break;
            default:
               return;
        }

        if ($isAlpha) {
            imagepalettetotruecolor($image);
            imagealphablending($image, true);
            imagesavealpha($image, true);
        }
        Storage::disk($attach->disk)->delete($attach->physicalPath());
        $outputPath = Storage::disk('public')->path(preg_replace('"\.(jpg|jpeg|png|webp)$"', '.webp', $attach->physicalPath()));
        imagewebp($image, $outputPath, $quality);
        $attach->mime = 'image/webp';
        $attach->extension = 'webp';

        $attach->save();
    }
}
