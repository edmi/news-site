<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\PostLanguage;
use App\Models\User;
use Barryvdh\Debugbar\Facades\Debugbar;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Orchid\Attachment\File;
use Orchid\Attachment\Models\Attachment;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class SiteController extends Controller
{
    public function index() {
        $postCategories = [];
        $categories = Category::all();

        return view('index', [
            'posts' => Post::with(['category'])->latest()->take(7)->get(),
            'postCategories' => $postCategories,
            'postTrending' => Post::trending(14),
            'categories' => $categories,
            'seo' => $this->seo
        ]);
    }
}
