<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $seo = [];

    public function __construct()
    {
        $this->seo['title'] = __('SiteTitle');
        $this->seo['description'] = __('SiteDescription');
        $this->seo['image'] = config()->get('app.defaultSiteLogo');
    }
}
