<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class SitemapController extends Controller
{

    public function index(Request $r)
    {
        $files = new Filesystem();
        $posts = Post::all();
        $files->put("sitemap.xml", View::make('sitemap',  compact('posts')));
    }
}
