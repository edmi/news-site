<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;

class CategoryController extends Controller
{
    public function show(string $locale, string $slug)
    {
        $category = Category::where('slug',$slug)->firstOrFail();
        $posts =  Post::getLatestByCategory($category->id);
        $this->seo['title'] = __(Category::DEFAULT_TITLES[$category->name]) ?? __("SiteTitle");
        $this->seo['description'] = __(Category::DEFAULT_DESC[$category->name]) ?? __("SiteDescription");
        $seo = $this->seo;

        return view('category', compact('category','posts','seo'));
    }
}
