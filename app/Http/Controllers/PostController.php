<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\PostView;
use App\Orchid\Presenters\PostPresenter;

class PostController extends Controller
{
    public function show(string $locale, string $slug)
    {
        $post = Post::findBySlug($slug);
        PostView::createViewLog($post);
        $post = new PostPresenter($post);
        $this->seo['title'] = $post->title;
        $this->seo['description'] = substr(strip_tags($post->body),0,200);
        $this->seo['image'] = $post->firstImage->url();
        $seo = $this->seo;
        return view('post', compact('post', 'seo'));
    }
}
