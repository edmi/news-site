<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Orchid\Presenters\PostPresenter;
use Illuminate\Http\Request;

class ServicePageController extends Controller
{
    public function show(Request $request, string $locale, string $page)
    {
        $id = $page === 'about' ? Post::ABOUT_PAGE : Post::PRIVACY_PAGE;
        $post = Post::find($id);
        $post = new PostPresenter($post);
        return view('post', compact('post'));
    }
}
