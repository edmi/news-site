<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure(Request): (Response|RedirectResponse) $next
     * @return Response|RedirectResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function handle(Request $request, Closure $next)
    {
        //расплата за неправильные изначальные локали
        $localeChanges = [
            'jp' => 'ja',
            'kp' => 'ko',
            'posts'=>'it/posts',
            'category'=>'it/category',
        ];

        $urlArray = explode('/', parse_url($request->url(), PHP_URL_PATH));
        $subdomain = $urlArray[1] ?? '';

        switch ($subdomain){
            case 'admin':
                app()->setLocale('ru_RU');
                return $next($request);
            case 'sitemap-generate.xml':
                return $next($request);
            case 'about':
            case 'privacy':
            app()->setLocale('it');
            return redirect(route('service',['it',$subdomain]), 301);
        }

        if (empty($subdomain) || strlen($subdomain) !== 2) {
            self::setLocale(app()->getLocale());
            return $next($request);
        }

        if ($subdomain === app()->getLocale() && count($urlArray) <= 2) {
            //return index if is default lang
            return redirect(route('index'), 301);
        }

        if (array_key_exists($subdomain, config()->get('app.locales'))) {
            self::setLocale($subdomain);
        } else {
            if (in_array($subdomain, array_keys($localeChanges))) {
                $newUrl = "/$localeChanges[$subdomain]";
                foreach ($urlArray as $key => $param) {
                    if ($key > 1) {
                        $newUrl .= '/' . $param;
                    }
                }
                return redirect($newUrl, 301);
            }
            abort('404');
        }

        return $next($request);
    }
    private static function setLocale($locale) {
        app()->setLocale($locale);
        setlocale(LC_TIME, $locale);
        url()->defaults(['locale' => $locale]);
    }
}
