<?php

namespace App\Providers;

use App\Models\Post;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts.parts.sideblock', function ($view) {
            $view->with('latestPost', Post::latest()->take(6)->get())
            ->with('trendingPost', Post::trending(14))
            ->with('popularPost', Post::trending());
        });
    }
}
