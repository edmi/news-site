<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\SitemapController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/sitemap-generate.xml', [SitemapController::class, 'index'])->name('sitemap');
Route::get('/service-page', [SiteController::class, 'siteService'])->name('site.service');
Route::get('/parsing', [SiteController::class, 'parsing'])->name('parsing');

Route::prefix('{locale}')->group(function () {
    Route::get('/{page}', [\App\Http\Controllers\ServicePageController::class, 'show'])
        ->whereIn('page', ['about', 'privacy'])
        ->name('service');
    Route::get('/posts/{slug}', [PostController::class, 'show'])->name('post.show');
    Route::get('/category/{slug}', [CategoryController::class, 'show'])->name('category.show');
})->whereIn('locale', config()->get('app.locales'));
Route::get('/{locale?}', [SiteController::class, 'index'])->name('index');

