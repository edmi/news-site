@extends('layouts.main')
@section('content')
    <section class="single-post-content" itemscope itemtype="http://schema.org/Article">
        <div class="container">
            <div class="row">
                <div class="col-md-9 post-content aos-init aos-animate" data-aos="fade-up">
                    <div class="single-post">
                        <div class="post-meta d-flex align-items-center">
                            <div class="flex-fill me-auto">
                                <div class="post-info" itemprop="publisher" itemscope
                                     itemtype="http://schema.org/Organization">
                                    <meta itemprop="name" content="Yyaloo">
                                    <meta itemprop="url" content="https://yyaloo.com">
                                    <meta itemprop="logo" content="{{ asset('/images/favicon.png') }}">
                                    <span class="date">{{__($post->category->name)}}</span>
                                    <span class="mx-1">•</span> <span itemprop="datePublished"
                                                                      content="{{$post->created_at}}">{{$post->created_at}} </span>
                                </div>
                            </div>
                            <div class="author d-flex align-items-center author-info" itemscope
                                 itemtype="http://schema.org/Person" itemprop="author">
                                <div class="name flex-grow-1 ms-3">
                                    <span class="me-2 p-0"
                                          itemprop="name">John Kim</span>
                                </div>

                                <div class="photo flex-shrink-0">
                                    <img src="{{ asset('/images/JohnAvatar50.jpg')}}" width="25" height="25"
                                         alt="John Kim avatar" itemprop="image"
                                         class="img-fluid rounded-circle">
                                </div>

                            </div>
                        </div>
                        <h1 itemprop="headline" class="mb-4">{!! $post->title !!}</h1>
                        <meta itemprop="name" content="{!! $post->title !!}">
                        <meta itemprop="datePublished" content="{{$post->created_at}}">

                        @if($post->category->name !== \app\Models\Category::SERVICE_CATEGORY)
                            <figure class="my-4 text-center" itemprop="image" itemscope
                                    itemtype="http://schema.org/ImageObject">
                                <meta itemprop="representativeOfPage" content="1">
                                <meta itemprop="height" content="{{$post->firstImage->height}}">
                                <meta itemprop="width" content="{{$post->firstImage->width}}">
                                <meta itemprop="url"
                                      content="{{$post->firstImage->url(asset(config()->get('app.noimage')))}}">
                                <img src="{{$post->firstImage->url()}}" alt="{!! $post->title !!}"
                                     width="{{$post->firstImage->width}}"
                                     height="{{$post->firstImage->height}}" class="img-fluid">
                            </figure>
                        @endif

                        <div class="inserts">
                            <!-- media in post yyaloo.com -->
                            <ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-7877942400302801"
                                 data-ad-slot="9647947002" data-ad-format="auto"
                                 data-full-width-responsive="true"></ins>
                            <script> (adsbygoogle = window.adsbygoogle || []).push({});</script>
                        </div>

                        <div itemprop="articleBody">
                            {!!  $post->body !!}
                        </div>

                        @if($post->postKb)
                            <h3>{{__("Download Update")}} {{$post->postKb}}</h3>
                            @if($post->postDownload)
                                <a href="{{$post->postDownload}}" target="blank"
                                   rel="nofollow noopener">{{__("Download")}} {{$post->postKb}}</a>

                            @endif
                        @endif

                        @if($post->postYoutube)
                            <iframe width="100%" height="450px"
                                    src="{{$post->postYoutube }}">
                            </iframe>
                        @endif
                    </div><!-- End Single Post Content -->
                    <div class="share pb-5">
                        <a href="http://twitter.com/share?text={!! $post->title !!}&url={{URL::toRoute(Route::current(),['locale' => app()->getLocale()] + Route::current()->parameters(), true)}}"
                           rel="nofollow"
                           onclick="window.open(this.href, 'twitter-share');return false;">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                 class="bi bi-twitter" viewBox="0 0 16 16">
                                <path
                                    d="M5.026 15c6.038 0 9.341-5.003 9.341-9.334 0-.14 0-.282-.006-.422A6.685 6.685 0 0 0 16 3.542a6.658 6.658 0 0 1-1.889.518 3.301 3.301 0 0 0 1.447-1.817 6.533 6.533 0 0 1-2.087.793A3.286 3.286 0 0 0 7.875 6.03a9.325 9.325 0 0 1-6.767-3.429 3.289 3.289 0 0 0 1.018 4.382A3.323 3.323 0 0 1 .64 6.575v.045a3.288 3.288 0 0 0 2.632 3.218 3.203 3.203 0 0 1-.865.115 3.23 3.23 0 0 1-.614-.057 3.283 3.283 0 0 0 3.067 2.277A6.588 6.588 0 0 1 .78 13.58a6.32 6.32 0 0 1-.78-.045A9.344 9.344 0 0 0 5.026 15z"/>
                            </svg>
                            <span>{{__("TwitterShare")}}</span>
                        </a>
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{URL::toRoute(Route::current(),['locale' => app()->getLocale()] + Route::current()->parameters(), true)}}"
                           rel="nofollow"
                           onclick="window.open(this.href, 'facebook-share');return false;">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                 class="bi bi-facebook" viewBox="0 0 16 16">
                                <path
                                    d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z"/>
                            </svg>
                            <span>{{__("FacebookShare")}}</span>
                        </a>

                    </div>
                </div>
                @include('layouts.parts.sideblock')
            </div>
        </div>
    </section>
@endsection

