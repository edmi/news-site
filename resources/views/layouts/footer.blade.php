<div class="footer-content">
    <div class="container">

        <div class="row g-5">
            <div class="col-lg-4">
                <h3 class="footer-heading">{{__("About")}} Yyaloo</h3>
                <p>© 2020 - {{ date('Y') }} Yyaloo - {{__("World news every day")}}</p>
            </div>
            <div class="col-6 col-lg-2">
                <h3 class="footer-heading">{{__("Navigation")}}</h3>
                <ul class="footer-links list-unstyled">
                    <li class="text-muted active">{{config()->get('app.locales')[app()->getLocale()]}}</li>
                    @foreach(getAviableLangs() as $lang => $name)
                        <li>@if(!empty(Route::current()))
                                <a href="{{URL::toRoute(Route::current(), ['locale' => $lang] + Route::current()->parameters(), true)}}">
                                    {{$name}}</a>
                            @else
                                <a href="{{route('index', ['locale' => $lang])}}">
                                    {{$name}}</a>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-6 col-lg-2">
                <h3 class="footer-heading">{{__("Categories")}}</h3>
                <ul class="footer-links list-unstyled">
                    <li><a href="{{route('category.show', 'news')}}"
                           title="{{__("High tech news")}}">{{__("High Tech")}}</a></li>
                    <li><a href="{{route('category.show', 'science')}}"
                           title="{{__("Science news")}}">{{__("Science")}}</a></li>
                    <li><a href="{{route('category.show', 'car')}}" title="{{__("Car news")}}">{{__("Car")}}</a></li>
                    <li><a href="{{route('category.show', 'game')}}" title="{{__("Game news")}}">{{__("Game")}}</a></li>
                    <li><a href="{{route('category.show', 'space')}}" title="{{__("Space news")}}">{{__("Space")}}</a>
                    </li>
                    <li><a href="{{route('category.show', 'crypto')}}"
                           title="{{__("Cryptocurrency news")}}">{{__("Cryptocurrency")}}</a></li>

                </ul>
            </div>
        </div>

    </div>
</div>

<div class="footer-legal">
    <div class="container">

        <div class="row justify-content-between">
            <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
                <div class="copyright">
                    © Copyright <strong><span>Yyaloo</span></strong>. {{__("All Rights Reserved")}}
                </div>
            </div>
            <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
                <a class="text-white"
                   href="{{route('service',['locale'=>app()->getLocale(),'page'=>'about'])}}">{{__("About us")}}</a> |
                <a class="text-white"
                   href="{{route('service', ['locale'=>app()->getLocale(),'page'=>'privacy'])}}">{{__("Privacy Policy")}}</a>
            </div>

        </div>
        <!--LiveInternet counter-->
        <a href="//www.liveinternet.ru/click" rel="noopener"
           target="_blank"><img id="licnt08E6" width="88" height="15" style="border:0"
                                title="LiveInternet"
                                src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAEALAAAAAABAAEAAAIBTAA7"
                                alt="visit_counter"></a>
        <script>(function (d, s) {
                d.getElementById("licnt08E6").src =
                    "//counter.yadro.ru/hit?t24.2;r" + escape(d.referrer) +
                    ((typeof (s) == "undefined") ? "" : ";s" + s.width + "*" + s.height + "*" +
                        (s.colorDepth ? s.colorDepth : s.pixelDepth)) + ";u" + escape(d.URL) +
                    ";h" + escape(d.title.substring(0, 150)) + ";" + Math.random()
            })
            (document, screen)</script><!--/LiveInternet-->
    </div>
</div>
