<title>{{$seo['title'] ?? __('SiteTitle')}}</title>
<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="{{$seo['description'] ?? __('SiteDescription')}}">

<meta content="width=device-width, initial-scale=1.0" name="viewport">
@if(!empty(Route::current()))
    @foreach(getAviableLangs() as $lang => $name)
        <link rel="alternate"
              href="{{URL::toRoute(Route::current(), ['locale' => $lang] + Route::current()->parameters(), true)}}"
              hreflang="{{$lang}}">
    @endforeach

    <link rel="canonical"
          href="{{URL::toRoute(Route::current(),['locale' => app()->getLocale()] + Route::current()->parameters(), true)}}">
    <meta itemprop="url"
          content="{{URL::toRoute(Route::current(),['locale' => app()->getLocale()] + Route::current()->parameters(), true)}}">

    <meta property="og:title" content="{{$seo['title'] ?? __('SiteTitle')}}">
    <meta property="og:description" content="{{$seo['description'] ?? __('SiteDescription')}}">
    <meta property="og:image" content="{{$seo['image'] ?? asset(config()->get('app.noimage'))}}">
    <meta itemprop="image" content="{{$seo['image'] ?? asset(config()->get('app.noimage'))}}">
    <meta name="twitter:image" content="{{$seo['image'] ?? asset(config()->get('app.noimage'))}}">
    <meta property="og:type" content="article">
    <meta name="author" content="John Kim">
    <meta property="og:site_name" content="Yyaloo">
    <meta property="og:url"
          content="{{URL::toRoute(Route::current(),['locale' => app()->getLocale()] + Route::current()->parameters(), true)}}">
@endif

<link rel="shortcut icon" href="{{ asset('/favicon.ico') }}">
<link href="{{ asset('/images/favicon.png') }}" rel="apple-touch-icon">

<!-- Google Fonts -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

<link rel="preload"
      href="https://fonts.googleapis.com/css2?family=EB+Garamond:wght@400;500&family=Inter:wght@400;500&family=Playfair+Display:ital,wght@0,400;0,700;1,400;1,700&display=swap"
      as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=EB+Garamond:wght@400;500&family=Inter:wght@400;500&family=Playfair+Display:ital,wght@0,400;0,700;1,400;1,700&display=swap">
</noscript>

<!-- Vendor CSS Files -->
<link rel="preload" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" as="style"
      onload="this.onload=null;this.rel='stylesheet'">
<noscript>
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
</noscript>

<!-- Template Main CSS Files -->
<link rel="preload" href="{{ asset('css/variables.css')}}" as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript>
    <link rel="stylesheet" href="{{ asset('css/variables.css')}}">
</noscript>

<link rel="preload" href="{{ asset('css/styles.pure.css')}}" as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript>
    <link rel="stylesheet" href="{{ asset('css/styles.pure.css')}}">
</noscript>
<script >
    function downloadJSAtOnload() {
        var element = document.createElement("script");
        element.src = "https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-7877942400302801";
        document.body.appendChild(element);
    }
    if (window.addEventListener)
        window.addEventListener("load", downloadJSAtOnload, false);
    else if (window.attachEvent)
        window.attachEvent("onload", downloadJSAtOnload);
    else window.onload = downloadJSAtOnload;
</script>
{{--<script defer async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-7877942400302801"--}}
{{--        crossorigin="anonymous"></script>--}}
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-R5CPPBVQ10"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());
    gtag('config', 'G-R5CPPBVQ10');
</script>
