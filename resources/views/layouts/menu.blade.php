<!-- ******HEADER****** -->
<nav id="navbar" class="navbar">
    <ul>
        <li><a href="{{route('category.show', 'news')}}" title="{{__("High tech news")}}">{{__("High Tech")}}</a></li>
        <li><a href="{{route('category.show', 'science')}}" title="{{__("Science news")}}">{{__("Science")}}</a></li>
        <li><a href="{{route('category.show', 'car')}}" title="{{__("Car news")}}">{{__("Car")}}</a></li>
        <li><a href="{{route('category.show', 'game')}}" title="{{__("Game news")}}">{{__("Game")}}</a></li>
        <li><a href="{{route('category.show', 'space')}}" title="{{__("Space news")}}">{{__("Space")}}</a></li>
        <li><a href="{{route('category.show', 'crypto')}}" title="{{__("Cryptocurrency news")}}">{{__("Cryptocurrency")}}</a></li>
    </ul>
</nav><!-- .navbar -->
