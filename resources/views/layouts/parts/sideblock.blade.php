<div class="col-md-3">
    <!-- ======= Sidebar ======= -->

    <div class="aside-block">

        <ul class="nav nav-pills custom-tab-nav mb-4" id="pills-tab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="pills-popular-tab" data-bs-toggle="pill"
                        data-bs-target="#pills-popular" type="button" role="tab" aria-controls="pills-popular"
                        aria-selected="true">{{__("Popular")}}</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="pills-trending-tab" data-bs-toggle="pill" data-bs-target="#pills-trending"
                        type="button" role="tab" aria-controls="pills-trending" aria-selected="false"
                        tabindex="-1">{{__("Trending")}}</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="pills-latest-tab" data-bs-toggle="pill" data-bs-target="#pills-latest"
                        type="button" role="tab" aria-controls="pills-latest" aria-selected="false"
                        tabindex="-1">{{__("Latest")}}</button>
            </li>
        </ul>

        <div class="tab-content" id="pills-tabContent">

            <!-- Popular -->
            <div class="tab-pane fade show active" id="pills-popular" role="tabpanel"
                 aria-labelledby="pills-popular-tab">
                @include('layouts.parts.post-grid-item',['items'=>$popularPost, 'noimage' => true, 'border' => true])
            </div> <!-- End Popular -->
            <!-- Trending -->
            <div class="tab-pane fade" id="pills-trending" role="tabpanel" aria-labelledby="pills-trending-tab">

                @include('layouts.parts.post-grid-item',['items'=>$trendingPost, 'noimage' => true, 'border' => true])
            </div> <!-- End Trending -->

            <!-- Latest -->
            <div class="tab-pane fade" id="pills-latest" role="tabpanel" aria-labelledby="pills-latest-tab">
                @include('layouts.parts.post-grid-item',['items'=>$latestPost, 'noimage' => true, 'border' => true])

            </div> <!-- End Latest -->

        </div>
    </div>
</div>
