<section class="category-section">
    <div class="container" data-aos="fade-up">
        <div class="section-header d-flex justify-content-between align-items-center mb-5">
            <h2>{{__($category->name)}}</h2>

            <div><a href="{{route('category.show',$category->slug)}}" class="more">
                    {{__("See All")}} {{__($category->name)}}</a></div>
        </div>
        <div class="row g-5">
            <div class="@if($loop->even === true)order-2 @endif col-lg-4">
                @include('layouts.parts.post-grid-item',['post'=>$categoryPost->first(), 'large'=>true, 'smalltext'=>100])
                @include('layouts.parts.post-grid-item',['items'=>$categoryPost->slice(1,2), 'noimage' => true, 'catName'=>$category->name])

            </div>
            <div class="col-lg-8">
                <div class="row g-5">
                    <div
                        class="col-lg-4 @if($loop->even === true)border-end order-last @else border-start @endif custom-border">
                        @include('layouts.parts.post-grid-item',['items'=>$categoryPost->slice(3,3), 'catName'=>$category->name])
                    </div>
                    <div class="col-lg-4 @if($loop->even === true)border-end @else border-start @endif custom-border">
                        @include('layouts.parts.post-grid-item',['items'=>$categoryPost->slice(6,3)])
                    </div>
                    <div
                        class="col-lg-4 @if($loop->even === true)border-end order-first @else border-start @endif custom-border">
                        @include('layouts.parts.post-grid-item',['items'=>$categoryPost->slice(9), 'noimage' => true, 'catName'=>$category->name])
                    </div>
                </div>
            </div>
        </div> <!-- End .row -->
    </div>
</section>
