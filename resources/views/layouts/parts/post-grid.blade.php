<section id="posts" class="posts">
    <div class="container" data-aos="fade-up">
        <div class="row g-5">

            <div class="col-lg-4">
                @include('layouts.parts.post-grid-item',['post'=>$posts->first(), 'large'=>true, 'smalltext'=>120])
            </div>

            <div class="col-lg-8">
                <div class="row g-5">
                    <div class="col-lg-4 border-start custom-border">
                        @include('layouts.parts.post-grid-item',['items'=>$posts->slice(1,3)])
                    </div>
                    <div class="col-lg-4 border-start custom-border">
                        @include('layouts.parts.post-grid-item',['items'=>$posts->slice(4,7)])
                    </div>
                    <!-- ======= Post Grid Section ======= -->

                    <!-- Trending Section -->
                    <div class="col-lg-4">

                        <div class="trending">
                            <h3>{{__("Trending")}}</h3>
                            <ul class="trending-post">
                                @foreach($postTrending as $post)
                                    <li>
                                        <a href="{{ route('post.show', $post->slug) }}">
                                            <span class="number">{{$loop->index +1}}</span>
                                            <h3>{!! $post->presenter()->title !!}</h3>
                                        </a>
                                    </li>
                                @endforeach


                            </ul>
                        </div>

                    </div> <!-- End Trending Section -->
                </div>
            </div>

        </div> <!-- End .row -->
    </div>
</section> <!-- End Post Grid Section -->
