@if(!isset($items))
    @include('layouts.parts.post-prev')
@else
    @foreach($items as $post)
        @include('layouts.parts.post-prev', ['border' => !$loop->last])
    @endforeach

@endif

