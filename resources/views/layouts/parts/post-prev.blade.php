<div class="post-entry-1 @isset($large) lg @endisset @isset($border) border-bottom @endisset">
    @if(!isset($noimage))
        <a href="{{ route('post.show', $post->slug) }}">
            @if($image = $post->firstImage)
                <img src="{{$image->url(asset(config()->get('app.noimage')))}}"
                     width="{{$image->width}}"
                     height="{{$image->height}}"
                     alt="{{$post->presenter()->title}}"
                     loading="lazy" class="img-fluid">
            @endif
        </a>
    @endif
    <div class="post-meta">
        <span class="date">{{__($catName ?? $post->category->name)}}</span>
        <span class="mx-1">&bullet;</span> <span>{{$post->presenter()->created_at}}</span>
    </div>
    <h2><a href="{{ route('post.show', $post->slug) }}">{!! $post->presenter()->title !!}</a></h2>
    @isset($smalltext)
        <div
            class="mb-4 d-block"> {!! force_balance_tags(Str::words($post->presenter()->body, $smalltext ?? 120,'...'))  !!}</div>
    @endisset
</div>
