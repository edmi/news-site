@extends('layouts.main')
@section('content')
    <section>
        <div class="container">
            <div class="row">

                <div class="col-md-9 aos-init aos-animate" data-aos="fade-up">
                    <h3 class="category-title">{{__("Category")}}: {{__($category->name)}}</h3>
                    @foreach($posts as $post)
                        <div class="d-md-flex post-entry-2 half">
                            <a href="{{route('post.show', [$post->slug])}}" class="me-4 thumbnail">
                                <img src="{{$post->firstImage->url(asset(config()->get('app.noimage')))}}"
                                     width="{{$post->firstImage->width }}"
                                     height="{{$post->firstImage->height}}" alt="{!! $post->presenter()->title !!}"
                                     loading="lazy"
                                     class="img-fluid">
                            </a>
                            <div class="col-md-6 p-4">
                                <div class="post-meta">
                                    <span class="date"> {{$category->name}}</span> <span class="mx-1">•</span>
                                    <span>{{$post->presenter()->created_at}}</span>
                                </div>
                                <h3>
                                    <a href="{{route('post.show', [$post->slug])}}">{!! $post->presenter()->title !!}</a>
                                </h3>
                                <div> {!! force_balance_tags(Str::words(strip_tags($post->presenter()->body,['<p>','<br>','<b>','<i>','< p>']), 100,'...'))  !!}</div>
                            </div>
                        </div>
                    @endforeach

                    <div class="text-start py-4">
                        <div class="custom-pagination">

                            {{$posts->links('pagination::bootstrap-5')}}
                        </div>
                    </div>
                </div>
                @include('layouts.parts.sideblock')

            </div>
        </div>
    </section>
@endsection
