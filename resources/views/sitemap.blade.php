<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:xhtml="http://www.w3.org/1999/xhtml">
    @foreach ($posts as $post)
        <url>
            <loc>{{route('post.show', ['it', $post->slug])}}</loc>
            @foreach(config()->get('app.locales') as $lang => $locale)
                @if($lang !== 'it')
                    <xhtml:link
                        rel="alternate"
                        hreflang="{{$lang}}"
                        href="{{route('post.show', [$lang, $post->slug])}}"/>
                @endif
            @endforeach
            <lastmod>{{ gmdate('Y-m-d\TH:i:s\Z',strtotime($post->updated_at)) }}</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach
</urlset>

