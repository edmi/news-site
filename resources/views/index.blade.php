@extends('layouts.main')
@section('content')
    @include('layouts.parts.post-grid')

    <!-- ======= IndexPage Category Section ======= -->
    @foreach($categories as $category)
        @if(count($categoryPost = $category->latestposts()) > 0 && $category->name !== \App\Models\Category::SERVICE_CATEGORY)
            @include('layouts.parts.index-post-category')
        @endif
    @endforeach

@endsection
